package com.example.forecastdemo.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.forecastdemo.R;
import com.example.forecastdemo.databinding.FragmentCurrentWeatherBinding;
import com.example.forecastdemo.helper.ResourceUtil;
import com.example.forecastdemo.helper.TimeUtils;
import com.example.forecastdemo.model.WeatherForecast;

import org.parceler.Parcels;
public class FragmentCurrentWeather extends Fragment
{
    private static final String KEY_EXTRA_WEATHER = "weather";
    private FragmentCurrentWeatherBinding binding;

    public static FragmentCurrentWeather getInstance(WeatherForecast weather)
    {
        FragmentCurrentWeather fragmentCurrentWeather = new FragmentCurrentWeather();
        Bundle extras = new Bundle();
        extras.putParcelable(KEY_EXTRA_WEATHER, Parcels.wrap(weather));
        fragmentCurrentWeather.setArguments(extras);
        return fragmentCurrentWeather;
    }

    @Nullable
    @Override
    public View onCreateView(
            @NonNull
                    LayoutInflater inflater,
            @Nullable
                    ViewGroup container,
            @Nullable
                    Bundle savedInstanceState)
    {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_current_weather, container, false);
        if(getArguments() != null && getArguments().containsKey(KEY_EXTRA_WEATHER))
        {
            WeatherForecast weatherForecast = Parcels.unwrap(getArguments().getParcelable(KEY_EXTRA_WEATHER));
            if(weatherForecast != null)
            {
                binding.setData(weatherForecast);
                binding.textViewDate.setText(TimeUtils.getTime(weatherForecast.getCurrently().getTime()));
                int drawableId = ResourceUtil.getImageResourceId(getActivity(), weatherForecast.getCurrently().getIcon().replaceAll("-", "_"));
                if(drawableId > 0)
                {
                    binding.imageViewIcon.setImageDrawable(ContextCompat.getDrawable(getActivity(), drawableId));
                }
            }
        }
        return binding.getRoot();
    }
}

//clear-day, clear-night, rain, snow, sleet, wind, fog, cloudy, partly-cloudy-day, or partly-cloudy-night
