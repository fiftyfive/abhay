package com.example.forecastdemo.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.forecastdemo.R;
import com.example.forecastdemo.adapter.ForecastAdapter;
import com.example.forecastdemo.databinding.FragmentForecastWeatherBinding;
import com.example.forecastdemo.model.WeatherForecast;

import org.parceler.Parcels;
public class FragmentForecast extends Fragment
{
    private static final String KEY_EXTRA_WEATHER = "weather";
    private FragmentForecastWeatherBinding binding;

    public static FragmentForecast getInstance(WeatherForecast weather)
    {
        FragmentForecast fragmentForecast = new FragmentForecast();
        Bundle extras = new Bundle();
        extras.putParcelable(KEY_EXTRA_WEATHER, Parcels.wrap(weather));
        fragmentForecast.setArguments(extras);
        return fragmentForecast;
    }

    @Nullable
    @Override
    public View onCreateView(
            @NonNull
                    LayoutInflater inflater,
            @Nullable
                    ViewGroup container,
            @Nullable
                    Bundle savedInstanceState)
    {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_forecast_weather, container, false);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        if(getArguments() != null && getArguments().containsKey(KEY_EXTRA_WEATHER))
        {
            WeatherForecast weatherForecast = Parcels.unwrap(getArguments().getParcelable(KEY_EXTRA_WEATHER));
            if(weatherForecast != null)
            {
                binding.recyclerView.setLayoutManager(layoutManager);
                ForecastAdapter adapter = new ForecastAdapter(getContext(), weatherForecast.getHourly().getData());
                binding.recyclerView.setAdapter(adapter);
            }
        }
        return binding.getRoot();
    }
}
