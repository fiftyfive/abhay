package com.example.forecastdemo.network;

import com.example.forecastdemo.BuildConfig;
import com.github.aurae.retrofit2.LoganSquareConverterFactory;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
public class RetrofitService
{
    private static final String BASE_URL = "https://api.darksky.net/";
    private static final long DEFAULT_CONNECTION_READ_TIMEOUT = 90;
    private static final long DEFAULT_WRITE_TIMEOUT = 90;
    private static final long DEFAULT_CONNECTION_TIMEOUT = 90;

    public Retrofit getRetrofit()
    {
        final OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();

        okHttpClientBuilder.readTimeout(DEFAULT_CONNECTION_READ_TIMEOUT, TimeUnit.SECONDS);
        okHttpClientBuilder.writeTimeout(DEFAULT_WRITE_TIMEOUT, TimeUnit.SECONDS);
        okHttpClientBuilder.connectTimeout(DEFAULT_CONNECTION_TIMEOUT, TimeUnit.SECONDS);

        // if debug true it will print log for api calling
        if(BuildConfig.DEBUG)
        {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            // set your desired log level
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            okHttpClientBuilder.interceptors().add(logging);
        }
        okHttpClientBuilder.addInterceptor(new Interceptor()
        {
            @Override
            public Response intercept(Chain chain) throws IOException
            {
                Request original = chain.request();
                Request request = original.newBuilder().build();
                return chain.proceed(request);
            }
        });

        // Adding Api caching interceptors
        final OkHttpClient okHttpClient = okHttpClientBuilder.build();

        return new Retrofit.Builder()
                // Base URL
                .baseUrl(BASE_URL)
                // JSON response conversion factory.
                .addConverterFactory(LoganSquareConverterFactory.create())
                // Rx Factory for callbacks
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                // Client to send the request to network
                .client(okHttpClient).build();
    }
}
