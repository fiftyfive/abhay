package com.example.forecastdemo.network;

import com.example.forecastdemo.model.WeatherForecast;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
public interface ApiCallInterface<T>
{
    @GET("forecast/{developerKey}/{latitude},{longitude}")
    Observable<WeatherForecast> getCurrentWeather(
            @Path("developerKey")
                    String developerKey,
            @Path("latitude")
                    String latitude,
            @Path("longitude")
                    String longitude,
            @Query("units")
                    String units);
}
