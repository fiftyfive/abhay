package com.example.forecastdemo.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.forecastdemo.R;
import com.example.forecastdemo.helper.ResourceUtil;
import com.example.forecastdemo.helper.TimeUtils;
import com.example.forecastdemo.model.WeatherForecast;

import java.util.List;
public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ForeCastItem>
{
    private Context context;
    private List<WeatherForecast.Hourly.Data> forecastList;

    public ForecastAdapter(Context context, List<WeatherForecast.Hourly.Data> forecastList)
    {
        this.context = context;
        this.forecastList = forecastList;
    }

    @NonNull
    @Override
    public ForeCastItem onCreateViewHolder(
            @NonNull
                    ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.item_hourly_forecast, viewGroup, false);
        return new ForeCastItem(view);
    }

    @Override
    public void onBindViewHolder(
            @NonNull
                    ForeCastItem viewHolder, int i)
    {
        WeatherForecast.Hourly.Data forecast = forecastList.get(i);
        if(forecast != null)
        {
            int drawableId = ResourceUtil.getImageResourceId(context, forecast.getIcon().replaceAll("-", "_"));
            if(drawableId > 0)
            {
                viewHolder.imageViewIcon.setImageDrawable(ContextCompat.getDrawable(context, drawableId));
            }
            viewHolder.textViewWeather.setText(forecast.getSummary());
            viewHolder.textViewTime.setText(TimeUtils.getTime(forecast.getTime()));
            viewHolder.textViewTemp.setText(String.format(context.getString(R.string.format_temp), forecast.getTemperature()));
        }
    }

    @Override
    public int getItemCount()
    {
        return forecastList.size();
    }

    class ForeCastItem extends RecyclerView.ViewHolder
    {
        private ImageView imageViewIcon;
        private TextView textViewWeather;
        private TextView textViewTime;
        private TextView textViewTemp;

        public ForeCastItem(
                @NonNull
                        View itemView)
        {
            super(itemView);
            imageViewIcon = itemView.findViewById(R.id.imageViewIcon);
            textViewWeather = itemView.findViewById(R.id.textViewWeather);
            textViewTime = itemView.findViewById(R.id.textViewTime);
            textViewTemp = itemView.findViewById(R.id.textViewTemp);
        }
    }
}
