package com.example.forecastdemo.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.forecastdemo.fragment.FragmentCurrentWeather;
import com.example.forecastdemo.fragment.FragmentForecast;
import com.example.forecastdemo.model.WeatherForecast;
public class FragmentAdapter extends FragmentStatePagerAdapter
{
    private int mNumTabs;
    private WeatherForecast model;

    public FragmentAdapter(FragmentManager fm, WeatherForecast model)
    {
        super(fm);
        mNumTabs = 2;
        this.model = model;
    }

    @Override
    public Fragment getItem(int position)
    {
        Fragment fragment = null;
        if(position == 0)
        {
            fragment = FragmentCurrentWeather.getInstance(model);
        }
        else if(position == 1)
        {
            fragment = FragmentForecast.getInstance(model);
        }
        return fragment;
    }

    @Override
    public int getCount()
    {
        return mNumTabs;
    }
}
