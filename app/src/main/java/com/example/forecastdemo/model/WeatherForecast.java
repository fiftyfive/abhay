package com.example.forecastdemo.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;
@Parcel
@JsonObject
public class WeatherForecast
{
    @JsonField
    private Currently currently;
    @JsonField
    private String offset;
    @JsonField
    private String timezone;
    @JsonField
    private String latitude;
    @JsonField
    private Hourly hourly;
    @JsonField
    private String longitude;

    public Currently getCurrently()
    {
        return currently;
    }

    public void setCurrently(Currently currently)
    {
        this.currently = currently;
    }

    public String getOffset()
    {
        return offset;
    }

    public void setOffset(String offset)
    {
        this.offset = offset;
    }

    public String getTimezone()
    {
        return timezone;
    }

    public void setTimezone(String timezone)
    {
        this.timezone = timezone;
    }

    public String getLatitude()
    {
        return latitude;
    }

    public void setLatitude(String latitude)
    {
        this.latitude = latitude;
    }

    public Hourly getHourly()
    {
        return hourly;
    }

    public void setHourly(Hourly hourly)
    {
        this.hourly = hourly;
    }

    public String getLongitude()
    {
        return longitude;
    }

    public void setLongitude(String longitude)
    {
        this.longitude = longitude;
    }

    @Parcel
    @JsonObject
    public static class Hourly
    {
        @JsonField
        private String summary;
        @JsonField
        private List<Data> data = new ArrayList<>();
        @JsonField
        private String icon;

        public String getSummary()
        {
            return summary;
        }

        public void setSummary(String summary)
        {
            this.summary = summary;
        }

        public List<Data> getData()
        {
            return data;
        }

        public void setData(List<Data> data)
        {
            this.data = data;
        }

        public String getIcon()
        {
            return icon;
        }

        public void setIcon(String icon)
        {
            this.icon = icon;
        }

        @Parcel
        @JsonObject
        public static class Data
        {
            @JsonField
            private String summary;
            @JsonField
            private String precipProbability;
            @JsonField
            private String visibility;
            @JsonField
            private String windGust;
            @JsonField
            private String precipIntensity;
            @JsonField
            private String icon;
            @JsonField
            private String cloudCover;
            @JsonField
            private String windBearing;
            @JsonField
            private String apparentTemperature;
            @JsonField
            private String pressure;
            @JsonField
            private String dewPoint;
            @JsonField
            private String ozone;
            @JsonField
            private String temperature;
            @JsonField
            private String humidity;
            @JsonField
            private long time;
            @JsonField
            private String windSpeed;

            public String getSummary()
            {
                return summary;
            }

            public void setSummary(String summary)
            {
                this.summary = summary;
            }

            public String getPrecipProbability()
            {
                return precipProbability;
            }

            public void setPrecipProbability(String precipProbability)
            {
                this.precipProbability = precipProbability;
            }

            public String getVisibility()
            {
                return visibility;
            }

            public void setVisibility(String visibility)
            {
                this.visibility = visibility;
            }

            public String getWindGust()
            {
                return windGust;
            }

            public void setWindGust(String windGust)
            {
                this.windGust = windGust;
            }

            public String getPrecipIntensity()
            {
                return precipIntensity;
            }

            public void setPrecipIntensity(String precipIntensity)
            {
                this.precipIntensity = precipIntensity;
            }

            public String getIcon()
            {
                return icon;
            }

            public void setIcon(String icon)
            {
                this.icon = icon;
            }

            public String getCloudCover()
            {
                return cloudCover;
            }

            public void setCloudCover(String cloudCover)
            {
                this.cloudCover = cloudCover;
            }

            public String getWindBearing()
            {
                return windBearing;
            }

            public void setWindBearing(String windBearing)
            {
                this.windBearing = windBearing;
            }

            public String getApparentTemperature()
            {
                return apparentTemperature;
            }

            public void setApparentTemperature(String apparentTemperature)
            {
                this.apparentTemperature = apparentTemperature;
            }

            public String getPressure()
            {
                return pressure;
            }

            public void setPressure(String pressure)
            {
                this.pressure = pressure;
            }

            public String getDewPoint()
            {
                return dewPoint;
            }

            public void setDewPoint(String dewPoint)
            {
                this.dewPoint = dewPoint;
            }

            public String getOzone()
            {
                return ozone;
            }

            public void setOzone(String ozone)
            {
                this.ozone = ozone;
            }

            public String getTemperature()
            {
                return temperature;
            }

            public void setTemperature(String temperature)
            {
                this.temperature = temperature;
            }

            public String getHumidity()
            {
                return humidity;
            }

            public void setHumidity(String humidity)
            {
                this.humidity = humidity;
            }

            public long getTime()
            {
                return time;
            }

            public void setTime(long time)
            {
                this.time = time;
            }

            public String getWindSpeed()
            {
                return windSpeed;
            }

            public void setWindSpeed(String windSpeed)
            {
                this.windSpeed = windSpeed;
            }
        }
    }

    @Parcel
    @JsonObject
    public static class Currently
    {
        @JsonField
        private String summary;
        @JsonField
        private String precipProbability;
        @JsonField
        private String visibility;
        @JsonField
        private String windGust;
        @JsonField
        private String precipIntensity;
        @JsonField
        private String icon;
        @JsonField
        private String cloudCover;
        @JsonField
        private String windBearing;
        @JsonField
        private String apparentTemperature;
        @JsonField
        private String pressure;
        @JsonField
        private String dewPoint;
        @JsonField
        private String ozone;
        @JsonField
        private String temperature;
        @JsonField
        private String humidity;
        @JsonField
        private long time;
        @JsonField
        private String windSpeed;

        public String getSummary()
        {
            return summary;
        }

        public void setSummary(String summary)
        {
            this.summary = summary;
        }

        public String getPrecipProbability()
        {
            return precipProbability;
        }

        public void setPrecipProbability(String precipProbability)
        {
            this.precipProbability = precipProbability;
        }

        public String getVisibility()
        {
            return visibility;
        }

        public void setVisibility(String visibility)
        {
            this.visibility = visibility;
        }

        public String getWindGust()
        {
            return windGust;
        }

        public void setWindGust(String windGust)
        {
            this.windGust = windGust;
        }

        public String getPrecipIntensity()
        {
            return precipIntensity;
        }

        public void setPrecipIntensity(String precipIntensity)
        {
            this.precipIntensity = precipIntensity;
        }

        public String getIcon()
        {
            return icon;
        }

        public void setIcon(String icon)
        {
            this.icon = icon;
        }

        public String getCloudCover()
        {
            return cloudCover;
        }

        public void setCloudCover(String cloudCover)
        {
            this.cloudCover = cloudCover;
        }

        public String getWindBearing()
        {
            return windBearing;
        }

        public void setWindBearing(String windBearing)
        {
            this.windBearing = windBearing;
        }

        public String getApparentTemperature()
        {
            return apparentTemperature;
        }

        public void setApparentTemperature(String apparentTemperature)
        {
            this.apparentTemperature = apparentTemperature;
        }

        public String getPressure()
        {
            return pressure;
        }

        public void setPressure(String pressure)
        {
            this.pressure = pressure;
        }

        public String getDewPoint()
        {
            return dewPoint;
        }

        public void setDewPoint(String dewPoint)
        {
            this.dewPoint = dewPoint;
        }

        public String getOzone()
        {
            return ozone;
        }

        public void setOzone(String ozone)
        {
            this.ozone = ozone;
        }

        public String getTemperature()
        {
            return temperature;
        }

        public void setTemperature(String temperature)
        {
            this.temperature = temperature;
        }

        public String getHumidity()
        {
            return humidity;
        }

        public void setHumidity(String humidity)
        {
            this.humidity = humidity;
        }

        public long getTime()
        {
            return time;
        }

        public void setTime(long time)
        {
            this.time = time;
        }

        public String getWindSpeed()
        {
            return windSpeed;
        }

        public void setWindSpeed(String windSpeed)
        {
            this.windSpeed = windSpeed;
        }
    }
}
