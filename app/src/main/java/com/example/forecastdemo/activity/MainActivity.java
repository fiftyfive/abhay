package com.example.forecastdemo.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.forecastdemo.R;
import com.example.forecastdemo.helper.AbstractMapLocationListener;
import com.example.forecastdemo.helper.HideUtil;
import com.example.forecastdemo.helper.LocationHelper;
import com.example.forecastdemo.helper.NetworkUtil;
import com.example.forecastdemo.helper.ToastUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener {

    private static final int CONST_DEFAULT_ZOOM = 11;
    private GoogleMap mMap;
    private Marker marker;
    private boolean isMyLocationButtonClicked;
    private TextView textViewAddress;
    private Disposable disposable;
    private Disposable disposableLatLng;
    private EditText searchView;
    private LatLng addressPoints;
    AbstractMapLocationListener locationListener = new AbstractMapLocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            super.onLocationChanged(location);
            LatLng myCurrentLocation = new LatLng(location.getLatitude(), location.getLongitude());
            addMarker(myCurrentLocation);
            ToastUtil.getInstance().showSnackBar(textViewAddress, getString(R.string.message_map_hint));
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        textViewAddress = findViewById(R.id.textViewAddress);
        searchView = findViewById(R.id.searchView);
        TextView textViewGetWeather = findViewById(R.id.textViewGetWeather);
        textViewGetWeather.setOnClickListener(this);
        View searchImage = findViewById(R.id.icon);
        final ImageView back = findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        searchImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HideUtil.hideSoftInput(new WeakReference<Activity>(MainActivity.this));
                if (!TextUtils.isEmpty(searchView.getText().toString().trim())) {
                    getLatLongFromAddress(searchView.getText().toString());
                } else {
                    ToastUtil.getInstance().showSnackBar(textViewAddress, getString(R.string.enter_location));
                }
            }
        });
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                addMarker(latLng);
            }
        });
        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                if (isMyLocationButtonClicked) {
                    addMarker(mMap.getCameraPosition().target);
                    isMyLocationButtonClicked = false;
                }
            }
        });
        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                isMyLocationButtonClicked = true;
                return false;
            }
        });
        Permissions.check(this, Manifest.permission.ACCESS_FINE_LOCATION, getString(R.string.rational_permission_message_location),
                new PermissionHandler() {
                    @Override
                    public void onGranted() {
                        mMap.setMyLocationEnabled(true);
                        LocationHelper locationTracker = new LocationHelper(MainActivity.this);
                        locationTracker.setLocationListener(locationListener);
                        locationTracker.getLocation();
                    }
                });
    }

    private void addMarker(final LatLng latLng) {
        if (latLng == null) {
            return;
        }
        addressPoints = latLng;
        if (marker != null) {
            marker.remove();
        }
        marker = mMap.addMarker(new MarkerOptions().position(latLng));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        if (mMap.getCameraPosition().zoom < CONST_DEFAULT_ZOOM) {
            mMap.animateCamera(CameraUpdateFactory.zoomTo(CONST_DEFAULT_ZOOM));
        }

        Single<String> addressObj = Single.fromCallable(new Callable<String>() {
            @Override
            public String call() throws Exception {
                if (NetworkUtil.isNetworkAvailable(MainActivity.this)) {
                    try {
                        Geocoder geocoder = new Geocoder(MainActivity.this, Locale.getDefault());
                        List<Address> addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                        return (addresses != null && addresses.size() > 0) ? addresses.get(0).getAddressLine(0) : "";
                    } catch (Exception e) {
                        throw new Exception(e.getMessage());
                    }
                }
                return "";
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());

        disposable = addressObj.subscribe(new Consumer<String>() {
            @Override
            public void accept(String s) {
                textViewAddress.setText(s);
            }
        });
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(MainActivity.this, ForecastActivity.class);
        intent.putExtra("address", ((TextView) view).getText());
        if (null != addressPoints) {
            intent.putExtra("lat", addressPoints.latitude);
            intent.putExtra("longi", addressPoints.longitude);
        }
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        HideUtil.hideSoftInput(new WeakReference<>(MainActivity.this));
        if (disposable != null) {
            disposable.dispose();
        }
        if (disposableLatLng != null) {
            disposableLatLng.dispose();
        }
    }

    private void getLatLongFromAddress(final String strAddress) {
        disposableLatLng = Single.fromCallable(new Callable<LatLng>() {
            @Override
            public LatLng call() throws Exception {
                Geocoder coder = new Geocoder(MainActivity.this);
                List<Address> address;
                LatLng p1 = null;

                try {
                    address = coder.getFromLocationName(strAddress, 5);
                    if (address == null) {
                        return new LatLng(0.0, 0.0);
                    }
                    Address location = address.get(0);
                    addressPoints = p1 = new LatLng((location.getLatitude()), (location.getLongitude()));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return p1 == null ? new LatLng(0.0, 0.0) : p1;
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<LatLng>() {
            @Override
            public void accept(LatLng latLng) throws Exception {
                if (latLng.longitude == 0.0 && latLng.latitude == 0.0) {
                    ToastUtil.getInstance().showSnackBar(textViewAddress, getString(R.string.invalid_address));
                } else {
                    addMarker(latLng);
                }
            }
        });
    }
}

