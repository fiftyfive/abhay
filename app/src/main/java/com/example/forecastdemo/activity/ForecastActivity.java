package com.example.forecastdemo.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.forecastdemo.R;
import com.example.forecastdemo.adapter.FragmentAdapter;
import com.example.forecastdemo.databinding.ActivityForecastBinding;
import com.example.forecastdemo.helper.AbstractTabSelectedListener;
import com.example.forecastdemo.model.WeatherForecast;
import com.example.forecastdemo.network.ApiCallInterface;
import com.example.forecastdemo.network.RetrofitService;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
public class ForecastActivity extends AppCompatActivity
{

    private Disposable disposable;
    private ActivityForecastBinding binding;

    @Override
    protected void onCreate(
            @Nullable
                    Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forecast);
        binding.slidingTabs.addTab(binding.slidingTabs.newTab().setText(getString(R.string.tab_current)));
        binding.slidingTabs.addTab(binding.slidingTabs.newTab().setText(getString(R.string.tab_forecast)));
        bindListeners();
        getCurrentWeather();
    }

    private void bindListeners()
    {
        binding.slidingTabs.addOnTabSelectedListener(new AbstractTabSelectedListener()
        {
            @Override
            public void onTabSelected(TabLayout.Tab tab)
            {
                super.onTabSelected(tab);
                binding.viewpager.setCurrentItem(tab.getPosition());
            }
        });
        binding.viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {

            }

            @Override
            public void onPageSelected(int position)
            {
                if(null != binding.slidingTabs.getTabAt(position))
                {
                    binding.slidingTabs.getTabAt(position).select();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state)
            {

            }
        });
    }

    private void getCurrentWeather()
    {
        if(getIntent().getExtras() != null)
        {
            binding.progress.setVisibility(View.VISIBLE);
            double latitude = getIntent().getDoubleExtra("lat", 0);
            double longitude = getIntent().getDoubleExtra("longi", 0);
            RetrofitService retrofitService = new RetrofitService();
            ApiCallInterface<WeatherForecast> service = retrofitService.getRetrofit().create(ApiCallInterface.class);
            disposable = service.getCurrentWeather(getString(R.string.developer_key), String.valueOf(latitude), String.valueOf(longitude),
                                                   "si")
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<WeatherForecast>()
                    {
                        @Override
                        public void accept(WeatherForecast weatherForecast) throws Exception
                        {
                            binding.progress.setVisibility(View.GONE);
                            FragmentAdapter adapter = new FragmentAdapter(getSupportFragmentManager(), weatherForecast);
                            binding.viewpager.setAdapter(adapter);
                        }
                    });
        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if(disposable != null)
        {
            disposable.dispose();
        }
    }
}
