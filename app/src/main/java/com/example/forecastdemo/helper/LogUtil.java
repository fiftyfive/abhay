package com.example.forecastdemo.helper;

import android.util.Log;

import com.example.forecastdemo.BuildConfig;

import java.io.PrintWriter;
import java.io.StringWriter;

public final class LogUtil
{
    /**
     * Priority constant for the println method; use Log.v.
     */
    public static final int VERBOSE = Log.VERBOSE;
    /**
     * Priority constant for the println method; use Log.d.
     */
    public static final int DEBUG = Log.DEBUG;
    /**
     * Priority constant for the println method; use Log.i.
     */
    public static final int INFO = Log.INFO;
    /**
     * Priority constant for the println method; use Log.w.
     */
    public static final int WARN = Log.WARN;
    /**
     * Priority constant for the println method; use Log.e.
     */
    public static final int ERROR = Log.ERROR;

    private static final String TAG = LogUtil.class.getSimpleName();

    private LogUtil()
    {
    }

    public static void log(Object msg)
    {
        log(TAG, msg);
    }

    public static void log(int logType, Object msg)
    {
        log(logType, TAG, msg);
    }

    public static void log(String tag, Object msg)
    {
        log(DEBUG, tag, msg);
    }

    public static void log(int logType, String tag, Object msg)
    {
        if(BuildConfig.DEBUG)
        {
            String message = "";
            if(msg != null)
            {
                message = msg.toString();
            }
            switch(logType)
            {
                case DEBUG:
                    Log.d(tag, message);
                    break;
                case ERROR:
                    Log.e(tag, message);
                    break;
                case INFO:
                    Log.i(tag, message);
                    break;
                case VERBOSE:
                    Log.v(tag, message);
                    break;
                case WARN:
                    Log.w(tag, message);
                    break;
                default:
                    Log.d(tag, message);
                    break;
            }
        }
    }

    public static void log(Throwable exception)
    {
        log(TAG, "", exception);
    }

    public static void log(int logType, Throwable exception)
    {
        log(logType, TAG, "", exception);
    }

    public static void log(Object msg, Throwable exception)
    {
        log(TAG, msg, exception);
    }

    public static void log(int logType, Object msg, Throwable exception)
    {
        log(logType, TAG, msg, exception);
    }

    public static void log(String tag, Object msg, Throwable exception)
    {
        log(DEBUG, tag, msg, exception);
    }

    public static void log(int logType, String tag, Object msg, Throwable exception)
    {
        if(BuildConfig.DEBUG)
        {
            StringBuilder builder = new StringBuilder();
            builder.append(tag).append(", Message: ");
            if(msg != null)
            {
                builder.append(msg.toString());
            }
            else
            {
                builder.append("");
            }
            if(exception != null)
            {
                StringWriter writer = new StringWriter();
                exception.printStackTrace(new PrintWriter(writer));
                builder.append(", Exception: ").append(writer.toString());
            }
            switch(logType)
            {
                case DEBUG:
                    Log.d(tag, builder.toString());
                    break;
                case ERROR:
                    Log.e(tag, builder.toString());
                    break;
                case INFO:
                    Log.i(tag, builder.toString());
                    break;
                case VERBOSE:
                    Log.v(tag, builder.toString());
                    break;
                case WARN:
                    Log.w(tag, builder.toString());
                    break;
                default:
                    Log.d(tag, builder.toString());
                    break;
            }
        }
    }
}