package com.example.forecastdemo.helper;

import android.content.Context;
public final class ResourceUtil
{
    private ResourceUtil()
    {
    }

    public static int getStringResourceId(Context context, String imageName)
    {
        return getResourceId(context, imageName, "string");
    }

    public static int getImageResourceId(Context context, String imageName)
    {
        return getResourceId(context, imageName, "drawable");
    }

    private static int getResourceId(Context context, String resourceName, String resourceType)
    {
        return context.getResources().getIdentifier(resourceName, resourceType, context.getPackageName());
    }
}
