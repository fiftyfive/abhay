package com.example.forecastdemo.helper;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.example.forecastdemo.R;

public final class ToastUtil
{
    private static ToastUtil ourInstance = new ToastUtil();

    private ToastUtil()
    {
    }

    public static final ToastUtil getInstance()
    {
        if(ourInstance == null)
        {
            ourInstance = new ToastUtil();
        }
        return ourInstance;
    }

    public void showSnackBar(View view, String message)
    {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    public void showToastMessage(final Context context, final String msg)
    {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                if(!TextUtils.isEmpty(msg))
                {
                    if(null != context)
                    {
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    if(null != context && null != context.getResources())
                    {
                        Toast.makeText(context, context.getResources().getString(R.string.wrong_output), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, 200);
    }
}
