package com.example.forecastdemo.helper;

import java.util.Calendar;
public final class TimeUtils
{

    public static String getTime(long timeMillis)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeMillis * 1000);
        String amOrPm = calendar.get(Calendar.AM_PM) == Calendar.AM ? "AM" : "PM";
        //months starts from 0 so here we need to add +1for actual month.
        String month = String.valueOf(calendar.get(Calendar.MONTH) +1);

        String time = "" + calendar.get(Calendar.HOUR) + ":" + calendar.get(Calendar.MINUTE) + " " + amOrPm + " (" + calendar.get(
                Calendar.DAY_OF_MONTH) + "/" + month + " )";
        return time;
    }
}
