package com.example.forecastdemo.helper;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.text.TextUtils;

import java.lang.ref.WeakReference;

public final class NetworkUtil
{
    /**
     * default constructor
     */
    public static final int CALL_PERMISSION_REQUEST_CODE = 17;

    private NetworkUtil()
    {
    }

    /**
     * @param ctx
     *         Context
     *
     * @return boolean true if network is available for device either WIFI or Mobile
     */
    public static boolean isNetworkAvailable(Context ctx)
    {
        WeakReference<Context> context = new WeakReference<>(ctx);
        boolean isConnected = false;
        ConnectivityManager cm = (ConnectivityManager) context.get().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm != null ? cm.getActiveNetworkInfo() : null;
        if(null != activeNetwork)
        {
            if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
            {
                isConnected = true;
            }
            if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
            {
                isConnected = true;
            }
        }
        else
        {
            isConnected = false;
        }
        return isConnected;
    }

}
